# Exercises #

### Field Guide ###

Fields of the exercise vary depending on exercise type and class. Addition of a new exercise type may add new fields. The exercise content JSON object should contain all the needed data for viewing and evaluating the exercise. This field guide presents only the fields that are included in the chapters.json request.

Field | Type | Description
------|------|-------------
class | String | Automatically created. Possible values: other, listening, comprehension, and listeningComprehension.
classAttribute | String | See class. Used for more convenient alternative in front-end development.
exerciseId | String | Used as a client-side unique identifier.
exerciseID|Int|Originally used for as an unique identifier inside the chapter
exercise_type|String|Type of the exercise. Automatically created. Possible values: basic, choose-from-options, fill-the-gap, prefilled-gap, write-heard, connect, translation, imageExercise,  and crossword.
instructions|String|Defined by the content producer. Instructions for how to complete the exercise.
questions|Collection of questions|
title|String|Title of the exercise.
type|String|Always “exercise”
sid|Int|Server side id. Used since `exercise_id` may not be unique outside the chapter.

### Question ###

Field|Type|Description
-----|----|----------
correct_answer|String|**Only used in crossword!**
correct_answers|Collection of correct answers|Is absent in the if the `exercise_type` is prefilled-gap , crossword or choose-from-options.
horizontal|Boolean|**Only used in crossword!** Defines if the word is down (“false”) or across (“true”) word.
options|Collections of Options|Only exists if the `exercise_type` is prefilled-gap or choose-from-options.
question|String|
questionID|String|Unique identifier for the question
x||**Only used in crossword!** Horizontal starting position of the answer from left side.
y||**Only used in crossword!** Vertical starting position of the answer from top side.

### Correct Answer ###

Field|Type|Description
-----|----|-----------
answer|String|
caseSensitive|Boolean|If there should be a distinction between letter casing (e.g. “Capital letter” is correct and “capital letter” is incorrect.) 
fieldID|Int|Only in fill-the-gap

### Options ###

Field|Type|Description
-----|----|----------
fieldID|Int|Only in pre-filled gap!
answer|String|Predifened answer to the question.
isCorrect|Boolean|Defines if the option is a correct one.

## Exercise Types ##
### Basic Exercise ###
#### basic ####
Contains a question and an input element where the answer shall be written. 
![Basic Exercise](images/basic.png)
```json
{
    "exercise_type":"basic",
    "title":"This is the Title of the Basic Exercise",
    "instructions":"These are the instructions.",
    "classAttribute":"other",
    "exerciseId":"new_k4g",
    "class":"other",
    "questions":[
        {
            "question":"Here is the question.",
            "correct_answers":[
                {
                    "answer":"Here is the answer",
                    "caseSensitive":false
                }],
            "questionID":"qmgr6lipbzr"
        }],
    "exerciseID":29100,
    "type":"exercise",
    "sid":40806,
    "content_position":3
}
```
### Connect Exercise ###
#### connect ####

In connect exercise user has to select the pairs by clicking them.  In this exercise, the wrong and correct pairs will be viewed immediately.
![Connect Exercise](images/connect.png)
```json
{
    "exercise_type":"connect",
    "title":"This is the Title of the Connect Exercise",
    "instructions":"These are the instructions.",
    "classAttribute":"other",
    "exerciseId":"new_p7n2",
    "class":"other",
    "questions":[
        {
            "question":"Dog",
            "correct_answers":[
                {
                    "answer":"Koira",
                    "caseSensitive":true
                }],
            "questionID":"q8py11g5inf"
        },
        {
            "question":"Cat",
            "correct_answers":[
                {
                    "answer":"Kissa",
                    "caseSensitive":true
                }],
            "questionID":"q6cb0l0rbbi"
        },
        {
            "question":"Donkey",
            "correct_answers":[
                {
                    "answer":"Aasi",
                    "caseSensitive":true
                }],
            "questionID":"qvpxif05kw4"
        }],
        "exerciseID":29101,
        "type":"exercise",
        "sid":40807,
        "content_position":4
}
```
### Crossword ###
#### crossword ####

Crossword exercise view should contain the grid for the crossword and the hints (questions). Hints and the word in the grid should be indicated by numbering.
![Crossword Exercise](images/crossword.png)
```json
{
    "exercise_type": "crossword",
    "title": "Crossword",
    "instructions": "Fill the crossword",
    "exerciseId": 28886,
    "questions": [
        {
            "question": "West from England",
            "correct_answer": "WALES",
            "questionID": "qvrgczs8taa",
            "x": 0,
            "y": 0,
            "horizontal": "true"
        },
        {
            "question": "Fast and dextreous",
            "correct_answer": "AGILE",
            "questionID": "qmd78finclv",
            "x": 0,
            "y": 1,
            "horizontal": "true"
        },
        {
            "question": "Seller",
            "correct_answer": "VENDER",
            "questionID": "qm3c4juzp1g",
            "x": 0,
            "y": 2,
            "horizontal": "true"
        },
        {
            "question": "It is over. It has _",
            "correct_answer": "ENDED",
            "questionID": "qfwuuk7rhhh",
            "x": 0,
            "y": 3,
            "horizontal": "true"
        },
        {
            "question": "Roller _",
            "correct_answer": "DERBY",
            "questionID": "qofr85e80mn",
            "x": 1,
            "y": 4,
            "horizontal": "true"
        },
        {
            "question": "Forbid",
            "correct_answer": "BAN",
            "questionID": "qrbhpwo10ld",
            "x": 0,
            "y": 5,
            "horizontal": "true"
        },
        {
            "question": "Sine _",
            "correct_answer": "WAVE",
            "questionID": "qna8fsttv9h",
            "x": 0,
            "y": 0,
            "horizontal": "false"
        },
        {
            "question": "Sometimes hidden",
            "correct_answer": "AGENDA",
            "questionID": "qv0ruzgy8r0",
            "x": 1,
            "y": 0,
            "horizontal": "false"
        },
        {
            "question": "A common tree in parks",
            "correct_answer": "LINDEN",
            "questionID": "q1akfr61k7t",
            "x": 2,
            "y": 0,
            "horizontal": "false"
        },
        {
            "question": "Old and wise",
            "correct_answer": "ELDER",
            "questionID": "qmhy8ke5pl4",
            "x": 3,
            "y": 0,
            "horizontal": "false"
        },
        {
            "question": "Cradle for plants",
            "correct_answer": "SEEDBED",
            "questionID": "qazos3co3k8",
            "x": 4,
            "y": 0,
            "horizontal": "false"
        }
    ],
    "exerciseID": 28886,
    "type": "exercise",
    "sid": 40486,
    "content_position": 1
}
```
### Fill in the gap exercise ###
#### fill-the-gap ####
Usually consists of a sentence (question) with input elements as gaps (field) where the answers will be written.
![Fill in the Gap Exercise](images/fillinthegap.png)
```json
{
    "exercise_type": "fill-the-gap",
    "title": "This is the Title of the Fill in the Gap Exercise",
    "instructions": "These are the instructions.",
    "classAttribute": "other",
    "exerciseId": "new_x102n6w",
    "class": "other",
    "questions": [
        {
            "question": "Here is the Question which contains an [field:0] in the gap.",
            "correct_answers": [
                {
                    "fieldID": 0,
                    "answer": "answer"
                }
            ],
            "questionID": "qdk2sgnaiyi"
        }
    ],
    "exerciseID": 29104,
    "type": "exercise",
    "sid": 40810,
    "content_position": 7
}
```

### Multiple-Choice Exercise ###
#### choose-from-options ####
![Multiple-Choice Exercise](images/multiple_choice.png)
```json
{
     "exercise_type": "choose-from-options",
     "title": "This is the Title of the Multiple-Choice Exercise", 
     "instructions": "These are the instructions.",
     "classAttribute": "other",
     "exerciseId": "new_5hplgj",
     "class": "other",
     "questions": [
         {
             "question": "Question",
             "options": [
                 {
                     "text": "Worng answer",
                     "isCorrect": false
                 },
                 {
                     "text": "Correct answer",
                     "isCorrect": false
                 },
                 {
                     "text": "Another wrong answer",
                     "isCorrect": false
                 }
             ],
             "questionID": "qxmedf47y9v"
         }
     ],
     "exerciseID": 29103,
     "type": "exercise",
     "sid": 40809,
     "content_position": 6
 }

```

### Prefilled gap ###
#### prefilled-gap ####
Combination of the multiple-choice exercise and fill in the gap exercise. The answer is selected by using a drop-down list.
![Prefilled Gap Exercise](images/prefilledgap.png)
```json
{
     "exercise_type": "prefilled-gap",
     "title": "This is the Title of the Prefilled Gap Exercise", 
     "instructions": "These are the instructions.",
     "classAttribute": "other",
     "exerciseId": "new_mwc3jeb2",
     "class": "other",
     "questions": [
         {
             "question": "Here is the Question which 0] in the gap.",
             "options": [
                 {
                     "fieldID": 0,
                     "answer": "wrong answer",
                     "isCorrect": false
                 },
                 {
                     "fieldID": 0,
                     "answer": "answer",
                     "isCorrect": true
                 },
                 {
                     "fieldID": 0,
                     "answer": "another wrong answer",
                     "isCorrect": false
                 }
             ],
             "questionID": "q3ih51q9kkn"
         }
     ],
     "exerciseID": 29105,
     "type": "exercise",
     "sid": 40811,
     "content_position": 8
 }

```

### Image Exercise ###
#### imageExercise ####
![Image Exercise](images/image_exercise.png)
```json
{
      "exercise_type": "imageExercise",
      "title": "This is the Title of the Image Exercise",
      "instructions": "These are the instructions.",
      "classAttribute": "other",
      "exerciseId": "new_bai1q",
      "class": "other",
      "questions": [
          {
              "question":"https://www.promentor.fi/pmweb/image/mascot-login.png",
              "correct_answers": [
                  {
                      "answer": "Globe",
                      "caseSensitive": false
                  },
                  {
                      "answer": "This is the answer",
                      "caseSensitive": false
                  }
              ],
              "questionID": "qzxz9vazvg7"
          }
      ],
      "exerciseID": 29102,
      "type": "exercise",
      "sid": 40808,
      "content_position": 5
}
```


